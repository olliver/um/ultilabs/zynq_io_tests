#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>

#define OCM_SIZE 128 * 1024
#define OCM_LOC 0x40000000

#define READ 1
#define WRITE 2

int main(int argc, char* argv[]) {
	int err = 0;
	uint32_t i;
	void* ocm = NULL;
	uint32_t* buf;
	int memf;
	int mode;
	struct timespec start, end;

	printf("ocm: 128 KB @ 0x%x\n", OCM_LOC);

	if (argc > 1) {
		if (argv[1][0] == 'r')
			mode = READ;
		if (argv[1][0] == 'w')
			mode = WRITE;
	} else {
		mode = READ | WRITE;
	}

	memf = open("/dev/mem", O_RDWR | O_SYNC);
	if (memf < 0) {
		err = errno;
		fprintf(stderr, "Open %s\n", strerror(err));
		goto err_;
	}

	ocm = mmap(NULL, OCM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memf,
		   OCM_LOC);
	if (ocm == MAP_FAILED) {
		err = errno;
		fprintf(stderr, "mmap %s\n", strerror(err));
		goto err_close;
	}

	if (mode & WRITE) {
		printf("Writing to OCM ... ");
		clock_gettime(CLOCK_MONOTONIC, &start);
		for (buf = (uint32_t *)ocm, i = 0; i < OCM_SIZE / 4; i++, buf++)
			*buf = i; /* << 16; */
		clock_gettime(CLOCK_MONOTONIC, &end);
		printf("done. Write time: %f\n", (end.tv_nsec - start.tv_nsec) * 0.000000001);
	}

	if (mode & READ) {
		printf("Reading OCM ... ");
		clock_gettime(CLOCK_MONOTONIC, &start);
		for (buf = (uint32_t *)ocm, i = 0; i < OCM_SIZE / 4; i++, buf++) {
			if (*buf != i) /* (i << 16)) */
				fprintf(stderr, "ocm[%x] = %x\n", i, *buf);
		}
		clock_gettime(CLOCK_MONOTONIC, &end);
		printf("done. Read time: %f\n", (end.tv_nsec - start.tv_nsec) * 0.000000001);
	}
	printf("Done\n");

	munmap(ocm, OCM_SIZE);

err_close:
	if(memf > 0)
		close(memf);
err_:
	return err;
}
